\documentclass[a4paper, amsfonts, amssymb, amsmath, reprint, showkeys, nofootinbib, twoside]{revtex4-2}
\usepackage{multirow}
\usepackage[utf8]{inputenc}
\usepackage{tabularx}
\usepackage[skip=5pt, indent=5pt]{parskip}
\usepackage{nicefrac}
\usepackage{wasysym}
\usepackage[colorinlistoftodos, color=green!40, prependcaption]{todonotes}
\usepackage[pdftex, pdftitle={Article}, pdfauthor={Author}]{hyperref} % For hyperlinks in the PDF
%\setlength{\marginparwidth}{2.5cm}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{float}
\bibliographystyle{apsrev4-2}
\makeatletter
\def\Dated@name{Date : }%
%...
\def\andname{et}
\makeatother

\begin{document}

\title{DMA and Master Curve, Viscoelasticity}
\author{Alexandre De Cuyper}
\date{\today} % Leave empty to omit a date

\begin{abstract} 
  \textbf{Abstract} 
This paper presents a comprehensive analysis of PLA samples using Dynamic Mechanical Analysis (DMA) with varied infill densities, complemented by the building of a master curve from a Stress Relaxation experiment. The DMA assessment involves multiple tests, including Strain Sweep, Frequency Sweep, and Temperature Ramp, to investigate the mechanical properties of the material thoroughly. The primary objective is to establish correlations between infill density and mechanical characteristics.

However, the inherent unreliability of the 3D printing process necessitates additional scrutiny. To address potential discrepancies in theoretical and measured densities, a meticulous weighing process becomes imperative. Notably, certain samples exhibited anomalous traits based on their theoretical density, rectified by accurate mass measurements, revealing a more accurate correlation between infill density and mechanical properties.

Subsequently, a detailed exploration of master curve construction is undertaken, leveraging different coefficient calculation methods from various equations. Despite initial challenges with the Arrhenius law, the utilization of the Williams–Landel–Ferry equation yields good results. By optimizing coefficients, we achieve a refined master curve closely aligning with hand-calculated coefficients.

Our study emphasizes the importance of DMA techniques for material characterization and offers valuable insights into the intricate relationship between infill density and mechanical properties. The master curve analysis provides a great perspective on material behavior, facilitating a deeper understanding across a broader range of timescales.

\end{abstract}

\maketitle

\section{Introduciton}

In this parper we will look at the results of our analysis with a DMA of samples of PLA printed with different density as well as analysing the data for another experiment of Stress Relaxation.

We'll make a quick introduction to the DMA and do a battery of test to analyse the mechanical properties of our material, for this purpose we'll do a Strain Sweep, Frequency Sweep and Temperature Ramp. This will allow us to get in depth understanding of this material and try to find a correlation between the infill density and its mechanical properties. 

Secondly, we'll build a master curve from a dataset and this will allow us to understand the properties of the material at really short and long time that are unpractical to run experiments with. For this we will use different methods to build this master curve by calculating coefficients with different equations and see which one gives the best results.

\section{DMA}

Dynamic Mechanical Analysis (DMA) is a useful technique in material science, providing a thorough understanding of mechanical properties. 

By subjecting materials to oscillatory stress, DMA reveals key insights into viscoelastic behavior, aiding in the optimization of material designs. 

We used Orange PLA polymer with a triangles infill pattern printed in the shape of a rectangle with dimension 30 x 5 x 0.8 mm with a 3D printing machine.
Various infill density were used to vary the density of the material.

\subsection{Strain Sweep}

A strain sweep involves varying the applied strain to study how a material's response changes under different strain levels. This dynamic test is crucial in characterizing a material's deformation behavior across a range of strains, providing valuable insights into its mechanical properties.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_strain.png}
    \caption{Strain Sweep for different infill density}
\end{figure}

\subsubsection{Influence of the 3D printing process}

We noticed that if we tested the sample right after it was printed we got different results than 
if we let it cool down for a few hours/a day.

As can be seen on Figure \ref{DMA_20} and \ref{DMA_35}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_20.png}
    \caption{Strain Sweep for various 20\% infill density samples}
    \label{DMA_20}
\end{figure}

The triangles\_20\_strain and triangles\_20\_revisser are the same material but the former was removed and reinstalled on the clamp to see the variation due to the installation process. 
The sample was printed the day before the test to give it time to cool down.

As for the triangles\_20\_strain\_refondu, it was a new sample recently printed few minutes before and immediately installed on the clamp.

Looking at the graph, we can correlate this with the change in Storage Modulus, the oldest one having a higher value meaning it is behaving more like a solid than the most recent that has a lower value meaning a very slighty more viscous behaviour.

This observation make sense considering the temperature variation between the two samples due to the 3D printing process.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_35.png}
    \caption{Strain Sweep for various 35\% infill density samples}
    \label{DMA_35}
\end{figure}

The same tests were made but with samples with 35\% infill density, using the same naming convention on the graph. 


\subsection{Frequency Sweep}

A frequency sweep involves varying the frequency of an applied force or deformation to assess how a material responds at different rates. This test is fundamental in understanding the frequency-dependent behavior of materials, offering insights into their viscoelastic properties across a spectrum of frequencies.

The frequency sweep was performed from 100Hz to 0.1Hz
\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_freq.png}
    \caption{Frequency Sweep for different infill density}
    \label{freq}
\end{figure}

As we can see in Figure \ref{freq}, the Storage Modulus response is relatively flat for most of the frequencies swept. 

The 20 and 35\% infill are basically the same and the 50\% has a higher storage modulus 
which was expected since it has the highest density.

\subsection{Temperature ramp}
A temperature ramp is a controlled variation in temperature applied to a material. This method enables a comprehensive investigation of the material's mechanical response as a function of temperature. 

By systematically altering the temperature, DMA allows for the exploration of thermal transitions, such as glass transition and melting points, elucidating the viscoelastic characteristics of the material. 

The technique is valuable for understanding the dynamic behavior of polymers, composites, and other materials, offering essential insights into their thermal and mechanical properties.

The temperature ramp was performed from 35°C to 100°C at a rate of 2°C per minute and with two frequencies of 1Hz and 10Hz for each sample.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_ramp.png}
    \caption{Temperature Ramp for sample with 50\% infill density}
    \label{ramp}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_ramp_storage.png}
    \caption{Storage Modulus for different infill density samples during temperature ramp}
    \label{ramp_stor}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_ramp_loss.png}
    \caption{Loss Modulus for different infill density samples during temperature ramp}
    \label{ramp_loss}
\end{figure}

In Figure \ref{ramp_stor} is represented the Storage Modulus as the temperature goes up and again we notice that the 20 and 35\% 
samples are very close together while the 50\% has a higher amplitude.

The Loss Modulus as seen in Figure \ref{ramp_loss} starts inceasing at 55°C, peaks at about 65°C and decrease rapidily to zero right after.


Now looking at the $tan(\delta)$ componant in Figure \ref{ramp_delta} we can see when the glass transition happen for the different samples at the same temperature.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_ramp_delta.png}
    \caption{$tan(\delta)$ for different infill density sample}
    \label{ramp_delta}
\end{figure}


The theorical glass transition temperature of PLA is 60°C and we can see on the graph that it starts at 60°C but the peak is actually closer to 70-75°C and finish its transition at 90°C to become viscoelastic.

We can clearly see that the glass transition peak depends on both, the frenquency applied and the density of the material.

According to the TTS (Time-Temperature-Superposition) principle, an increase in frequency is analogous to an increase in temperature. Therefore, increasing the frequency tends to shift the glass transition peak to higher temperatures. This is known as a "frequency shift" or "shift factor."

The amplitude of the peaks is also affected by the applied frequency on the sample.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_ramp_50_delta.png}
    \caption{$tan(\delta)$ for 50\% infill density sample}
    \label{ramp_delta_50}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_ramp_35_delta.png}
    \caption{$tan(\delta)$ for 35\% infill density sample}
    \label{ramp_delta_35}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{plot/DMA_ramp_20_delta.png}
    \caption{$tan(\delta)$ for 20\% infill density sample}
    \label{ramp_delta_20}
\end{figure}

However, we cannot extrapolate these finding to find a correlation between infill density and the peak properties as the amplitude varies too much from one sample to another without a specific pattern.

In figure \ref{ramp_delta_50} (50\% infill) the first peak is higher, while in \ref{ramp_delta_35} (35\% infill) it is lower but in \ref{ramp_delta_20} (20\% infill) they both seem at the same amplitude.

Furthermore, as we've seen in Figure \ref{ramp_stor}, the amplitude of the Storage Modulus is almost the same for both 20\% and 35\% meaning that these two samples have very similar mechanical properties. Therefore the difference in amplitude in the peak of $tan(\delta)$ cannot be explained simply by the samples density.

\subsection{Mass and Density}

To understand this more in depth we'll be looking at the mass and density of the samples.

The mass of each sample were recorded as well and the data is compiled on Table \ref{tab:data}
\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Density (\%)} & \textbf{Weight (g)} \\
        \hline
        80 & 0.1302 \\
        50 & 0.1337 \\
        46 & 0.1121 \\
        42.5 & 0.1058 \\
        35 & 0.1024 \\
        20 & 0.0962 \\
        20 II & 0.0893 \\
        \hline
    \end{tabular}
    \caption{Mass (in g) of each sample printed}
    \label{tab:data}
\end{table}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/mass_density.png}
  \end{center}
  \caption{Plot of the mass of each sample}\label{mass}
\end{figure}

Looking at the linear fit for the mass of the sample, we could've expected a linear increase of the mass as the density increase
but instead what we get here is that there is a lot of variation from one sample to another.

We can see a trend however, the mass increases as the infill desnity increases but the data doesn't really fit a linear curve.

It looks like we reach a plateau after 50\% and the only difference is the printing variability, same can be said with 35\% to 20\%

Also, we printed two identical samples with 20\% infill density (in Table \ref{tab:data} 20 and 20 II in the Density (\%) column) to look at the variation from one printing to another and we can see a significant difference between the two when they should in theory be identical. 

This would explain some of the variations we got in our previous results, we still find a trend but the variations are too high to make precise predictions of the material properties from its theorical infill density value used in software. 

Also factors like temperature of the sample plays an important role in the viscoelastic properties of the materials and need to be mesearued precisely, as even small variations like testing right after printing or waiting until it reachs room temperature, make a huge difference on the results. 

\section{Master Curve}

The master curve is a fundamental concept in materials science, particularly in the context of dynamic mechanical analysis (DMA). It represents a method to unify and correlate time-temperature-dependent mechanical behavior across a range of conditions. 

By leveraging temperature and time shift factors, the master curve facilitates the extrapolation of material properties, enabling a comprehensive understanding of viscoelasticity and aiding in the prediction of material performance under diverse thermal and loading conditions. 

This approach also plays a crucial in understand the properties of the material at a very long or short time.

\subsection{Time–temperature superposition}

Time-Temperature Superposition (TTS) is a powerful technique employed in the field of materials science and rheology to analyze and predict the mechanical behavior of materials over a broad range of temperatures and time scales. 

TTS leverages the principle of shifting data acquired at different temperatures to create a master curve, providing a comprehensive understanding of a material's viscoelastic properties. 

This method enables the extrapolation of material responses, aiding in the design and optimization of materials for diverse applications. 

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/master_curve.png}
  \end{center}
  \caption{Stress relaxation data as a function of temperature}\label{mc}
\end{figure}

We manually calculated the coefficients by hand to shift the curve to make a master curve.
The coefficients applied to its curve are shown in the legend in Figure \ref{mc_coeff}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/master_curve_with_coeff.png}
  \end{center}
  \caption{Master curve generated by horizontally shifting data}\label{mc_coeff}
\end{figure}

We can see that the coefficients follow a linear profile very well.
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/coefficients_plot.png}
  \end{center}
  \caption{Coefficients manually calculated to fit the master curve}\label{coeff}
\end{figure}


\subsection{Arrhenius law}

The Arrhenius Law, a fundamental concept in chemical kinetics, plays a pivotal role in understanding the temperature-dependent behavior of materials. In the context of time-temperature superposition (TTS) and the master curve, the Arrhenius Law provides insights into the rate of thermally activated processes.

The Arrhenius Law is expressed as:
\begin{equation}
k = A \cdot e^{-\frac{E_a}{RT}}
\end{equation}
where:
\begin{align*}
k & : \text{Rate constant of the process} \\
A & : \text{Pre-exponential factor or frequency factor} \\
E_a & : \text{Activation energy} \\
R & : \text{Gas constant} \\
T & : \text{Temperature (in Kelvin)}
\end{align*}

In the master curve analysis, the Arrhenius Law is employed to relate the temperature-dependent material properties. By incorporating the Arrhenius Law, one can extrapolate material behavior over a wide temperature range, unifying diverse datasets into a single master curve. This enables the prediction of material response under different conditions and facilitates the exploration of long-term behavior.

The activation energy ($E_a$) and pre-exponential factor ($A$) derived from the Arrhenius Law become essential parameters in constructing the master curve. They govern the temperature sensitivity of the material, providing a framework for predicting behavior beyond the original experimental range.

Utilizing the Arrhenius Law within the master curve methodology allows us to extrapolate and interpolate material behavior efficiently, making it a valuable tool in the study of time-temperature-dependent processes.
\begin{equation}
  log(a_T) = -\frac{E_\alpha}{2.303 R}\left(\frac{1}{T} - \frac{1}{T_0}\right) 
\end{equation}
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/master_curve_with_coeff_arrhenius.png}
  \end{center}
  \caption{Master curve made with coefficients calculated with the Arrhenius law}\label{mc_coeff_arrhenius}
\end{figure}

Unfortunately when applying the coefficients calculated with the Arrhenius law to the curves to obtain the master curve
we do not get very good results so we need to try another method to build our master curve.


\subsection{Williams–Landel–Ferry equation}

The Williams-Landel-Ferry (WLF) equation is a fundamental model in polymer science, employed to describe the temperature dependence of material properties. Named after its contributors, the WLF equation provides insights into the dynamic response of polymers over a wide temperature range. It serves as a powerful tool for characterizing viscoelastic behavior, helping to comprehend the impact of temperature variations on the mechanical properties of polymers.

\begin{equation}
  log(a_T) = \frac{-C_1 (T - T_0)}{C_2 + (T - T_0)}
\end{equation}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/master_curve_with_coeff_calculated.png}
  \end{center}
  \caption{Coefficients calculated with WLF equation}\label{mc_coeff_calc}
\end{figure}

Here is the master curve with the coefficient calculated with the WLF equation using the coeffcients for 
a general polymer: $C_1 = 8.86$ and $C_2 = 101.6 K$ \cite{fujimoto1968stress}

This looks decent but we can still improve this result, as these coefficients can be changed to fit the empirical data depending on the kind of polymer used. 

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/master_curve_with_coeff_optimized.png}
  \end{center}
  \caption{Master curve made with coefficients from the WLF equation}\label{mc_coeff_opti}
\end{figure}
After optimizing the coefficients to fit the ones calculated by hand using a non-linear least squares technique to fit the equation parameters (C1 and C2) to the data (here, the manually calculated coefficients) to give the best empirical results

We get the following coefficients for $C_1 =  22.6$ and $C_2 = 158.4$

The coefficients were optimized to be the closest as possible to the one manually calculated and we can see on Figure \ref{mc_coeff_opti} that it is closer to the master curve we want. 

They're also close to the theorical one from the litterature.
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/master_curve_coeff_optimized_original.png}
  \end{center}
  \caption{Comparison between manually calculated and WLF coefficients}\label{mc_coeff_opti_orig}
\end{figure}

And comparing the two set of coefficients we can see in Figure \ref{mc_coeff_opti_orig} we can see that we get a really close correspondence between the two curves.

As we can see in Figure \ref{coeff_comparison_fit} the two curves fit really well with a linear function.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.48\textwidth]{plot/master_curve_coeff_optimized_original_fit.png}
  \end{center}
  \caption{Comparison between manually calculated and WLF coefficients linear fit}\label{mc_coeff_opti_orig_fit}
  \label{coeff_comparison_fit}
\end{figure}


\section{Conclusion}

To conclude, this study of different samples with the DMA and the analysis of the master curve showed us the importance of these techniques to characterize the properties of a material. 

We saw that the infill density does play a role in the mechanical properties of our material, however the 3D printing process isn't reliable enough and we need to weight the samples to see if we actually get the right density.
This the case for some samples that displays weird characteristics considering their theorical density, but if we correct it by using the actual measured density (by measuring the mass of the sample) we get a decent correlation between the denisty infill and the mechanical properties.

Another important thing that we noticed was this "plateau" effect above 50\% and below 35\%, meaning that our margin is actually quite small compared to what we could've expected. This is likely due to the wall effect, as our sample is small and the change in density is really just a change in the pattern of the 3D printing inside the sample.

This means that above or below theses plateau the changes don't have a noticable impact on the properties of the material.

Secondly, we looked at master curves, understanding what it is, why they are useful and how to build them. 
Manually calculating the coefficients worked really well but as we wanted a more robust and general approach we used the Arrhenius law and the Williams–Landel–Ferry equation to calculate these coefficients.

The Arrhenius law didn't give us good results and the master curve obtained wasn't useful but the WLF equation gave decent results using general coefficients from the litterarure and by optimizing them to be closer to our hand calculated coefficients we get really good result

\bibliography{references.bib}
\end{document}
