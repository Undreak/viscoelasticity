import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress
from scipy.optimize import curve_fit

# Assuming 'data' is your DataFrame
pairs = 11  # Number of pairs

# Create a list to store matrices/vectors
matrices = []

# Read the Excel file
file_path = "StressRelaxationDataForTTSPractice20220205.xlsx"
data = pd.read_excel(file_path, skiprows=1)
temperatures = pd.read_excel(file_path, header=None, nrows=1)
temps = np.zeros((temperatures.shape[1] - 1 ) // 2)

for i in range((len(temps))):
    temps[i] = temperatures [2*i + 1]

print(temps)

# Iterate through each pair
for i in range(pairs):
    # Extract log(t) and log E(t) for the current pair
    log_t_col = f'log (t).{i}' if i > 0 else 'log (t)'
    log_E_t_col = f'log E(t).{i}' if i > 0 else 'log E(t)'
    
    # Create a matrix/vector for the current pair
    current_matrix = np.array(data[[log_t_col, log_E_t_col]])
    
    # Append the matrix/vector to the list
    matrices.append(current_matrix)

# Print or use the matrices as needed
for i, matrix in enumerate(matrices):
    print(f'Matrix/Vector {i+1}:\n{matrix}\n')

# Create a figure and axis
fig, ax = plt.subplots()

# Iterate through each matrix and plot scatter points
for i, matrix in enumerate(matrices):
    log_t, log_E_t = matrix[:, 0], matrix[:, 1]
    ax.plot(log_t, log_E_t, label=f'{temps[i]}°C', marker='o')

# Set labels and title
ax.set_xlabel('log (t)')
ax.set_ylabel('log E(t)')
ax.set_title('Master Curve')

# Display legend
ax.legend()

# Save the plot as an image (e.g., PNG)
plt.savefig('plot/master_curve.png')

# Show the plot
plt.show()

coeff = [4.4, 3.05, 2.2, 1.5, 0.8, 0, -1, -2.1, -3, -3.9, -4.4]

# Create a figure and axis
fig, ax = plt.subplots()

# Iterate through each matrix and plot scatter points
for i, matrix in enumerate(matrices):
    log_t, log_E_t = matrix[:, 0], matrix[:, 1]

    # Add the corresponding coefficient to log(t) values
    log_t_with_coeff = log_t - coeff[i]

    ax.plot(log_t_with_coeff, log_E_t, label=f'{coeff[i]}', marker='o')

# Set labels and title
ax.set_xlabel('log (t)')
ax.set_ylabel('log E(t)')
ax.set_title('Master Curve with Coefficients')

# Display legend
ax.legend()

# Save the plot as an image (e.g., PNG)
plt.savefig('plot/master_curve_with_coeff.png')

# Show the plot
plt.show()

# Create an array for the x-axis (index of the coefficients)
x = np.arange(len(coeff))

# Perform linear fit
fit = np.polyfit(x, coeff, 1)
fit_line = np.poly1d(fit)

# Create a figure and axis
fig, ax = plt.subplots()

# Plot the original data
ax.scatter(x, coeff, label='Original Data')

# Plot the linear fit
ax.plot(x, fit_line(x), color='red', label=f'Linear Fit: {fit_line}')


# Set labels and title
ax.set_xlabel('Index')
ax.set_ylabel('Coefficient Values')
ax.set_title('Linear Fit of Coefficients')

# Display legend
ax.legend()

# Save the plot
plt.savefig('plot/coefficients_plot.png')

# Show the plot
plt.show()

# Constants
R = 8.314  # Ideal gas constant in J/(mol*K)

# Reference temperature (choose the temperature at which you want the master curve)
T_ref = temps[5] + 273.15  # in Kelvin

# Empty lists to store coefficients
pre_exponential_factors = []
activation_energies = []

# Empty lists to store transformed datasets
transformed_datasets = []

# Iterate through each matrix and calculate coefficients
for i, matrix in enumerate(matrices):
    if i != 5:
        # Filter out rows with nan values
        valid_rows = ~np.isnan(matrix).any(axis=1)
        matrix = matrix[valid_rows]

        # Extract log-transformed data
        log_t = matrix[:, 0]
        log_E_t = matrix[:, 1]

        # Calculate 1/T
        inverse_temperature = 1 / (temps[i] + 273.15) - 1/T_ref

        # Perform linear regression
        slope, intercept, _, _, _ = linregress(log_t, log_E_t)

        # Calculate Arrhenius coefficients
        pre_exponential_factor = fit_line[0]
        activation_energy = -fit_line[1] * R  

        # Append coefficients to the lists
        pre_exponential_factors.append(np.exp(pre_exponential_factor))
        activation_energies.append(activation_energy)

        # Transform the dataset to the reference temperature
        transformed_value = activation_energy * inverse_temperature/2.303

    else:
        transformed_value = 0
        pre_exponential_factor = 0

    # Store the transformed dataset
    transformed_datasets.append(pre_exponential_factor*transformed_value*2500)


# Create a figure and axis
fig, ax = plt.subplots()

# Iterate through each matrix and plot scatter points
for i, matrix in enumerate(matrices):
    log_t, log_E_t = matrix[:, 0], matrix[:, 1]

    # Add the corresponding coefficient to log(t) values
    log_t_with_coeff = log_t - transformed_datasets[i]
    ax.plot(log_t_with_coeff, log_E_t, label=f'{transformed_datasets[i]}', marker='o')

# Set labels and title
ax.set_xlabel('log (t)')
ax.set_ylabel('log E(t)')
ax.set_title('Master Curve with Arrhenius Coefficients')

# Display legend
ax.legend(['{:.2f}'.format(coeff) for coeff in transformed_datasets])

# Save the plot as an image (e.g., PNG)
plt.savefig('plot/master_curve_with_coeff_arrhenius.png')

# Show the plot
plt.show()


# Empty lists to store coefficients
pre_exponential_factors1 = []
activation_energies1 = []

# Empty lists to store transformed datasets
transformed_datasets1 = []
# Iterate through each matrix and calculate coefficients
for i, matrix in enumerate(matrices[:5]):
    # Filter out rows with nan values
    valid_rows = ~np.isnan(matrix).any(axis=1)
    matrix = matrix[valid_rows]

    # Extract log-transformed data
    log_t1 = matrix[:, 0]
    log_E_t1 = matrix[:, 1]

    # Calculate 1/T
    inverse_temperature = 1 / (temps[i] + 273.15) - 1/T_ref

    # Perform linear regression
    slope, intercept, _, _, _ = linregress(log_t1, log_E_t1)
    print(slope)

    # Calculate Arrhenius coefficients
    pre_exponential_factor1 = np.exp(intercept)
    activation_energy1 = -slope * R  

    # Append coefficients to the lists
    pre_exponential_factors1.append(pre_exponential_factor1)
    activation_energies1.append(activation_energy1)

    # Transform the dataset to the reference temperature
    transformed_value1 = activation_energy1 * inverse_temperature/2.303

    # Store the transformed dataset
    transformed_datasets1.append(pre_exponential_factor1*transformed_value1)

# Empty lists to store coefficients
pre_exponential_factors2 = []
activation_energies2 = []

# Empty lists to store transformed datasets
transformed_datasets2 = []
for i, matrix in enumerate(matrices[6:]):
    # Filter out rows with nan values
    valid_rows = ~np.isnan(matrix).any(axis=1)
    matrix = matrix[valid_rows]

    # Extract log-transformed data
    log_t2 = matrix[:, 0]
    log_E_t2 = matrix[:, 1]

    # Calculate 1/T
    inverse_temperature = 1 / (temps[i+6] + 273.15) - 1/T_ref

    # Perform linear regression
    slope, intercept, _, _, _ = linregress(log_t2, log_E_t2)
    print(slope)

    # Calculate Arrhenius coefficients
    pre_exponential_factor2 = np.exp(intercept)
    activation_energy2 = -slope * R  

    # Append coefficients to the lists
    pre_exponential_factors2.append(pre_exponential_factor2)
    activation_energies2.append(activation_energy2)

    # Transform the dataset to the reference temperature
    transformed_value2 = activation_energy2 * inverse_temperature/2.303

    # Store the transformed dataset
    transformed_datasets2.append(pre_exponential_factor2*transformed_value2)

coeff_arrhenius = transformed_datasets1 + [0] + transformed_datasets2
# Create a figure and axis
fig, ax = plt.subplots()

# Iterate through each matrix and plot scatter points
for i, matrix in enumerate(matrices):
    log_t, log_E_t = matrix[:, 0], matrix[:, 1]
    log_t_with_coeff = log_t - coeff_arrhenius[i]

    # Add the corresponding coefficient to log(t) values
    ax.plot(log_t_with_coeff, log_E_t, label=f'{coeff_arrhenius[i]}', marker='o')

# Set labels and title
ax.set_xlabel('log (t)')
ax.set_ylabel('log E(t)')
ax.set_title('Master Curve with Coefficients Corrected for Glass Transition')

# Display legend
ax.legend(['{:.2f}'.format(coeff) for coeff in transformed_datasets])

# Save the plot as an image (e.g., PNG)
plt.savefig('plot/master_curve_with_coeff_arrhenius_glass.png')

# Show the plot
plt.show()





ln_a = - ((8.86*(temps + 273.15 - T_ref))/(101.6 + temps + 273.15 - T_ref))
# Create a figure and axis
fig, ax = plt.subplots()

# Iterate through each matrix and plot scatter points
for i, matrix in enumerate(matrices):
    log_t, log_E_t = matrix[:, 0], matrix[:, 1]

    # Add the corresponding coefficient to log(t) values
    log_t_with_coeff = log_t - ln_a[i]
    ax.plot(log_t_with_coeff, log_E_t, label=f'{ln_a[i]}', marker='o')

# Set labels and title
ax.set_xlabel('log (t)')
ax.set_ylabel('log E(t)')
ax.set_title('Master Curve with Coefficients')

# Display legend
ax.legend(['{:.2f}'.format(coeff) for coeff in ln_a])

# Save the plot as an image (e.g., PNG)
plt.savefig('plot/master_curve_with_coeff_calculated.png')

# Show the plot
plt.show()

def wlf_equation(temps, a, b):
    return -(a * (temps + 273.15 - T_ref)) / (b + temps + 273.15 - T_ref)

# Fit the WLF equation to the data
popt, pcov = curve_fit(wlf_equation, (-np.array(coeff)), temps)

# Extract the optimized constants
a_opt, b_opt = popt

# Print the optimized constants
print(f"Optimized Constants: a = {a_opt}, b = {b_opt}")

# Generate the predicted ln_a values using the optimized constants
ln_a_optimized = wlf_equation(temps, -a_opt, b_opt)
# Perform linear fit
fit_opt = np.polyfit(x, ln_a_optimized, 1)
fit_line_opt = np.poly1d(fit_opt)

# Create a figure and axis
fig, ax = plt.subplots()

# Plot the original and optimized ln_a values
ax.plot(x, coeff, label='Original coefficients')
ax.plot(x, ln_a_optimized, label='Optimized WLF coefficients')

# Set labels and title
ax.set_xlabel('Index')
ax.set_ylabel('Coefficients')
ax.set_title('Original and Optimized Coefficients with Fit')

# Display legend
ax.legend()

# Save the plot as an image (e.g., PNG)
plt.savefig("plot/master_curve_coeff_optimized_original.png")

# Show the plot
plt.show()

# Create a figure and axis
fig, ax = plt.subplots()

# Plot the linear fit
ax.plot(x, fit_line(x), color='blue', label=f'Original Linear Fit: {fit_line}')
ax.plot(x, fit_line_opt(x), color='red', label=f'Optimized WLF Linear Fit: {fit_line_opt}')

# Set labels and title
ax.set_xlabel('Index')
ax.set_ylabel('Coefficients')
ax.set_title('Original and Optimized Coefficients with Fit')

# Display legend
ax.legend()

# Save the plot as an image (e.g., PNG)
plt.savefig("plot/master_curve_coeff_optimized_original_fit.png")

# Show the plot
plt.show()


# Create a figure and axis
fig, ax = plt.subplots()

# Iterate through each matrix and plot scatter points
for i, matrix in enumerate(matrices):
    log_t, log_E_t = matrix[:, 0], matrix[:, 1]

    # Add the corresponding coefficient to log(t) values
    log_t_with_coeff = log_t - ln_a_optimized[i]
    ax.plot(log_t_with_coeff, log_E_t, label=f'{ln_a[i]}', marker='o')

# Set labels and title
ax.set_xlabel('log (t)')
ax.set_ylabel('log E(t)')
ax.set_title('Master Curve with Coefficients Optimized')

# Display legend
ax.legend(['{:.2f}'.format(coeff) for coeff in ln_a_optimized])

# Save the plot as an image (e.g., PNG)
plt.savefig('plot/master_curve_with_coeff_optimized.png')

# Show the plot
plt.show()

# Data
percentages = [80, 50, 46, 42.5, 35, 20, 20]
mass_values = [0.1302, 0.1337, 0.1121, 0.1058, 0.1024, 0.0962, 0.0893]

# Define the linear fit function
def linear_fit(x, a, b):
    return a * x + b

# Perform the linear fit
params, covariance = curve_fit(linear_fit, percentages, mass_values)

# Extract the optimal values for the fit parameters
a, b = params

# Generate the equation of the fit
fit_equation = f'y = {a:.4f}x + {b:.4f}'

# Plotting the data and the linear fit
plt.scatter(percentages, mass_values, label='Data')
plt.plot(percentages, linear_fit(np.array(percentages), a, b), color='red', label=f'Linear Fit: {fit_equation}')

plt.xlabel('Density (%)')
plt.ylabel('Mass (g)')
plt.title('Density vs. Mass with Linear Fit')
plt.legend()
plt.savefig("plot/mass_density.png")
plt.show()
